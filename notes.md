# original email to Ralf

* General intro to speech perception as inference.  
* Lack of invariance: mapping from acoustic cues to categories changes based on context/talker.
* Adaptation, and a toy model of how you can implement it by changing readout/connection weights.
* Challenge of higher-level inference: can’t just continuously update connection weights based on recent experience, because you’d forget about familiar talkers that way.
* Computationally this is a pretty easy thing to accommodate in Bayesian inference: you just have a “lumpy” prior on accents (generative model parameters) that guides adaptation.  But it’s not so clear how you’d work this kind of thing into the sort of computational model that implements adaptation.

What I want to get across is a sense of some of the challenges of linking higher level cognitive/computational models with neural implementations, even when a lot of the work has already been done (e.g., can do learning/inference in predictive coding/PPC/etc.).  How does this sound?

# an outline

* intro
    * what is speech perception?
        * inference under uncertainty (plot of phonetic distributions)
        * ...in a _nonstationary environment_ (plot of talker dists).  "all I mean by 'nonstationary' is that the distributions change from situation to situation."
    * why study speech percpetion in particular?
        * (same could be said of _any_ kind of perception)
        * speech+language has _doubly-hierarchical structure_ (diagram from grant)
        * linguistic structure: cues/phonemes/words/sentences/etc.
        * more importantly, _indexical_ structure: situations/talkers/dialects/languages
        * we know a lot about _both_ kinds of structure, and we know that people are actually sensitive to these structurse and use them in the inference process.
        * moreover, they have big implications for understanding how perception-as-inference might work in the brain.
* speech perception as inference
    * most basic level: infer intended category given cue value
    * apply bayes rule, easy peasy
    * can implement neurally in any number of ways (PPCs, predictive coding, simulation, etc.)
* lack of invariance
    * cue distributions _change_ depending on whos talking etc.
    * these changes are _stylistic_ and require _life long learning_.
    * e.g., we are often meeting people with new accents.
* adaptation via belief-updating
    * at the simplest level, what we need to do is update our beliefs about generative model that's currently in use.
    * infer generative model _parameters_ (e.g. mean and variance).
    * there are lots of ways to implement parameter learning in neurally plausible frameworks
        * e.g., beliefs are encoded in _connection weights_ (either read-out weights for prediction weights).
        * and updating can occur based _solely_ on the prediction error (all local information).
        * (we can even to a certain extent modulate degree of prior confidence via learning rate).
* challenge of _higher-level_ inference.
    * OKAY GREAT WE DID IT!
    * but a problem becomes clear when we think about it at a (high) computational level.
    * what would happen if we just continuously updated beliefs/weights?
    * every time another talker starts speaking, you have to re-learn that talker's generative model/weights
    * ...and at the same time you'd _forget_ what you knew about the last talker
    * this might not be such a bad strategy when talker differences are just random.  you just jack up teh learning rate every time you notice that your current beliefs about the generative model aren't working so well.
    * but it's a _terrible_ strategy when you are going to re-encounter the same or similar talkers over and over again.  
    * want to be able to _hold on_ to your beliefs about individual talkers or groups of talkers, swapping them out when it's appropriate
    * there's lots of evidence that people _do_ do this in fact (talker familiarity effects, persistent adaptation, effects of top-down cues to talker identity or gender or dialect/accent)
    * and it's really really easy to do in the _computational level_ way of thinking: just make beliefs condition on "talker type", and infer talker type along with phonetic categorization and generative model parameters.
    * but it does _not_ fit neatly into the framework where parameters are encoded as connection weights, because it treats the parameters as another set of random variables
    * that is, beliefs about the _category_ are encoded via activity of "neurons" representing different possible categories or different functions of category identity.  it's easy to see how you can modulate your beliefs about categories by modulating the activities via normal inputs of various types.  You can "swap out" different beliefs about which category you're about to hear by just providing the right kind of input.  But beliefs about the _parameters_ are encoded in the _weights_, and it's not totally clear to me how you can modulate the weights in the same, rapid way.


intuition: you should update your beliefs way more after each observation when you don't know very much.
update rules depend on _other_ variables that are non-local.
